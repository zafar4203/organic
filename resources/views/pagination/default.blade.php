
@if ($paginator->lastPage() > 1)
<!-- Shop Breadcrumb Area Start -->
<div class="shop-breadcrumb-area border-default mt-30">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-5">
            <span class="show-items">Showing {{$paginator->firstItem()}}-{{$paginator->lastItem()}} of {{$paginator->total()}} item(s) </span>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-7">
            <ul class="pfolio-breadcrumb-list text-center">
                <li class="float-left prev {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}"><a href="{{ $paginator->url(1) }}"><i class="fa fa-angle-left"
                            aria-hidden="true"></i>Previous</a></li>

                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        <a class="pagination-url" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor

                <li class="float-right next {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}"><a href="{{ $paginator->url($paginator->currentPage()+1) }}">Next<i class="fa fa-angle-right"
                            aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- Shop Breadcrumb Area End -->

@endif
