<!doctype html>
<html class="no-js" lang="zxx">


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @if(isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
    		<title>{{$gs->title}}</title>
        @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
    		<title>{{$gs->title}}</title>
        @elseif(isset($productt))
      <meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
      <meta name="description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}">
	    <meta property="og:title" content="{{$productt->name}}" />
	    <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
	    <meta property="og:image" content="{{asset('assets/images/thumbnails/'.$productt->thumbnail)}}" />
	    <meta name="author" content="Manara">
    	<title>{{substr($productt->name, 0,11)."-"}}{{$gs->title}}</title>
        @else
        <meta name="keywords" content="{{ $seo->meta_keys }}">
	    <meta name="author" content="Manara">
		<title>{{$gs->title}}</title>
        @endif
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Oragnic Care Shop | @yield('title')</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/favicon/favicon.ico')}}" />


    <!-- Fontawesome css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/font-awesome.min.css') }}">
    <!-- Animate css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/animate.css') }}">
    <!-- 7-stroke fonts css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/pe-icon-7-stroke.min.css') }}">
    <!-- Nice select css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/nice-select.css') }}">
    <!-- Jquery fancybox css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/jquery.fancybox.css') }}">
    <!-- Jquery ui price slider css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/jquery-ui.min.css') }}">
    <!-- Meanmenu css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/meanmenu.min.css') }}">
    <!-- Owl carousel css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/owl.carousel.min.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/bootstrap.min.css') }}">
    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/default.css') }}">
    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/responsive.css') }}">
    <!-- Toastr css -->
    <link rel="stylesheet" href="{{ asset('public/assets/front/css/toastr.css') }}">

    <!-- Modernizer js -->
    <script src="{{ asset('public/assets/front/js/vendor/modernizr-3.5.0.min.js') }}"></script>
    <style>
        .custom-control {
            position: relative;
            display: inline-block;
            margin-left:10px;
            min-height: 1.5rem;
            padding-left: 1.5rem;
            padding-top:2px;
        }
        .custom-radio .custom-control-input:checked~.custom-control-label::before {
            background-color: #c7b270;
        }
        .add-offer-to-cart {
            color: #ffffff;
            display: inline-block;
            font-size: 14px;
            font-weight: 400;
            line-height: 30px;
            text-transform: capitalize;
        }
        .fa-heart {
            color:#c7b270;
        }
        #cart-count{
            background:#6ab950;
        }
        .add-to-cart{
            cursor:pointer;
        }
        .prods{
            height:265px;
        }
        .cart-header-body{
            max-height:250px;
            overflow-y:auto;
        }
        .pro-title a {
             white-space:normal !important;
             font-weight:500;
        }
        @media only screen and (max-width: 600px) {
            .header-top{
                display:none;
            }
            .logo-img{
                width:75%;
                height:50px !important;
            }
            .pro-img img {
                width: 70% !important;
                margin:0px auto;
                display:block !important;
                /* height: 100% !important; */
            }
            .offrs img{
                width: 100% !important;
            }
            .offrs + .pro-content a,.offrs + .pro-content p span{
                font-weight:bold;
                font-size:18px;
            }

            .actions-primary, .actions-secondary {
                opacity: 1;
                -webkit-transform: scale(0.6);
                transform: scale(1);
                -webkit-transition: all 0.5s ease-in-out 0s;
                transition: all 0.5s ease-in-out 0s;
            }
            .pro-actions { 
                opacity: 1!important;
                margin-top:0.5% !important;
                position:relative;
            }
            .prods {
                height: 220px;
            }
            /* add to cart btn style */
            .add-to-cart::before {
                display:none;
            }
            .add-to-cart{
                background:#c7b270;
                padding:3px 5px;
                border-radius:5px;
                color:white !important;
            }
        }

        /* width */
        ::-webkit-scrollbar {
            width: 8px;
            height: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: lightgray;
        }

       /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: darkgray;
        }

        .single-cart-box{
            margin-right:15px;
        }
    </style>
    @yield('styles')
</head>

<body class="template-color-1">
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!-- Main Wrapper Start Here -->
    <div class="wrapper">
        <!-- Newsletter Popup Start -->
        <!-- <div class="popup_wrapper">
            <div class="test">
                <span class="popup_off">Close</span>
                <div class="subscribe_area text-center mt-40">
                    <h2>Newsletter</h2>
                    <p>Subscribe to the Makali mailing list to receive updates on new arrivals, special offers and other
                        discount
                        information.</p>
                    <div class="subscribe-form-group">
                        <form action="#">
                            <input autocomplete="off" type="text" name="message" id="message" placeholder="Enter your email address">
                            <button type="submit">subscribe</button>
                        </form>
                    </div>
                    <div class="subscribe-bottom mt-15">
                        <input type="checkbox" id="newsletter-permission">
                        <label for="newsletter-permission">Don't show this popup again</label>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Newsletter Popup End -->
        <!-- Main Header Area Start Here -->

        @include('front.chunks.header')
        <input type="hidden" value="{{route('user.login')}}" id="user_login_url" />
        @yield('content')

        <!-- Footer Area Start Here -->
        @include('front.chunks.footer')
        <!-- Footer Area End Here -->

        <!-- Quick View Content Start -->
        <div class="main-product-thumbnail quick-thumb-content">
            <div class="container">
                <!-- The Modal -->
                <div class="modal fade" id="quickview" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="quick-view-modal">
                                
                                </div>
                            </div>
                            <!-- Modal footer -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Quick View Content End -->


        <!-- Offer Quick View Content Start -->
        <div class="main-product-thumbnail quick-thumb-content">
            <div class="container">
                <!-- The Modal -->
                <div class="modal fade" id="offerquickview" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="offer-quick-view-modal">
                                
                                </div>
                            </div>
                            <!-- Modal footer -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Offer Quick View Content End -->

    </div>
    <!-- Main Wrapper End Here -->

    <script type="text/javascript">
      var mainurl = "{{url('/')}}";
      var gs      = {!! json_encode($gs) !!};
      var langg    = {!! json_encode($langg) !!};
    </script>

    <!-- jquery 3.3.1 -->
    <script src="{{ asset('public/assets/front/js/vendor/jquery-3.3.1.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.18/jquery.zoom.min.js"></script>
    <!-- Countdown js -->
    <script src="{{ asset('public/assets/front/js/jquery.countdown.min.js') }}"></script>
    <!-- Mobile menu js -->
    <script src="{{ asset('public/assets/front/js/jquery.meanmenu.min.js') }}"></script>
    <!-- ScrollUp js -->
    <script src="{{ asset('public/assets/front/js/jquery.scrollUp.js') }}"></script>
    <!-- Fancybox js -->
    <script src="{{ asset('public/assets/front/js/jquery.fancybox.min.js') }}"></script>
    <!-- Jquery nice select js -->
    <script src="{{ asset('public/assets/front/js/jquery.nice-select.min.js') }}"></script>
    <!-- Jquery ui price slider js -->
    <script src="{{ asset('public/assets/front/js/jquery-ui.min.js') }}"></script>
    <!-- Owl carousel -->
    <script src="{{ asset('public/assets/front/js/owl.carousel.min.js') }}"></script>
    <!-- Bootstrap popper js -->
    <script src="{{ asset('public/assets/front/js/popper.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('public/assets/front/js/bootstrap.min.js') }}"></script>
    <!-- Plugin js -->
    <script src="{{ asset('public/assets/front/js/plugins.js') }}"></script>
    <!-- Main activaion js -->
    <script src="{{ asset('public/assets/front/js/main.js') }}"></script>

    <!-- Main Activation JS -->
    <script src="{{ asset('public/assets/front/js/main_a.js') }}"></script>
    <script src="{{ asset('public/assets/front/js/custom2.js') }}"></script>
    <script src="{{ asset('public/assets/front/js/toastr.js') }}"></script>

    @yield('scripts')
</body>



</html>