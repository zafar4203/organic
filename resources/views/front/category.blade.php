@extends('layouts.front')
@section('styles')
    <style>
    .disabled{
        pointer-events: none;
        }
        span#show-total-pp{
            padding:0;
        }
        #search-tag{
            border:1px solid #c7b270 ;
            padding:10px 5px;
            border-radius:5px;
        }
        #search-tag i{
            color:#c7b270;
        }
    </style>
@endsection

@section('content')

                    <!-- Breadcrumb Area Start Here -->
        <div class="breadcrumb-area">
            <div class="container">
                <ol class="breadcrumb breadcrumb-list">
                    <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="breadcrumb-item active">
                            @if(!empty($childcat)) 
                                {{ $childcat->name }} 
                            @elseif(!empty($subcat))
                                {{ $subcat->name }}
                            @elseif(!empty($cat))
                                {{ $cat->name }}
                            @else
                                Category
                            @endif
                    </li>
                </ol>
            </div>
        </div>
        <!-- Breadcrumb Area End Here -->
        <!-- Shop Page Start -->
        <div class="main-shop-page ptb-90">
            <div class="container">
                <!-- Row End -->
                <div class="row">

                    @include('includes.catalog')                        
                    <!-- Product Categorie List Start -->
                    <div class="col-lg-9 order-1 order-lg-2">

                    @include('includes.filter')
                        <!-- Grid & List View End -->
                        <div class="shop-area mb-all-40">
                            <!-- Grid & List Main Area End -->
                            <div class="tab-content">
                                <div id="grid-view" class="tab-pane fade show active">
                                    <div class="row border-hover-effect" id="ajaxContent">
                                       @include('includes.product.filtered-products')
                                    </div>
                                    <!-- Row End -->
                                </div>
                                <!-- #grid view End -->
                                <div id="list-view" class="tab-pane fade fix">
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/1.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Modern Eye Brush</a>
                                            </h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$55.50</span><span class="prev-price">$59.50</span>
                                            </p>
                                            <p>New Look eye Material with high neckline. Soft and stretchy material for
                                                a comfortable fit. Accessorize with a straw hat and you're ready for
                                                summer!</p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/2.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Sprite Yoga Straps1</a>
                                            </h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$29.99</span><span class="prev-price">$24.00</span>
                                            </p>
                                            <p>Accessorize with a straw hat and you're ready for summer! New Look eye
                                                Material with high neckline. Soft and stretchy material for a
                                                comfortable fit. </p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/3.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Voyage face cleaner</a>
                                            </h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$26.99</span><span class="prev-price">$25.50</span>
                                            </p>
                                            <p>Soft and stretchy material for a comfortable fit. Accessorize with a
                                                straw hat and you're ready for summer! New Look eye Material with high
                                                neckline. </p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/4.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Impulse Duffle</a></h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$45.50</span><span class="prev-price">$42.50</span>
                                            </p>
                                            <p>Soft and stretchy material for a comfortable fit. New Look eye Material
                                                with high neckline.Accessorize with a straw hat and you're ready for
                                                summer!</p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/5.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Field Messenger</a>
                                            </h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$20.00</span><span class="prev-price">$17.50</span>
                                            </p>
                                            <p>Soft and stretchy material for a comfortable fit. Accessorize with a
                                                straw hat and you're ready for summer! New Look eye Material with high
                                                neckline.</p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/8.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Impulse Duffle</a></h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$45.50</span><span class="prev-price">$42.50</span>
                                            </p>
                                            <p>Soft and stretchy material for a comfortable fit. New Look eye Material
                                                with high neckline.Accessorize with a straw hat and you're ready for
                                                summer!</p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                    <!-- Single Product Start Here -->
                                    <div class="single-makal-product">
                                        <div class="pro-img">
                                            <a href="product-details.html"><img src="{{ asset('public/assets/front/img/products/cosmetic/6.jpg')}}" alt="product-img"></a>
                                            <span class="sticker-new">new</span>
                                        </div>
                                        <div class="pro-content">
                                            <h4 class="pro-title"><a href="product-details.html">Field Messenger</a>
                                            </h4>
                                            <div class="rating">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            </div>
                                            <p><span class="price">$20.00</span><span class="prev-price">$17.50</span>
                                            </p>
                                            <p>Soft and stretchy material for a comfortable fit. Accessorize with a
                                                straw hat and you're ready for summer! New Look eye Material with high
                                                neckline.</p>
                                        </div>
                                    </div>
                                    <!-- Single Product End Here -->
                                </div>
                                <!-- #list view End -->
                            </div>
                            <!-- Grid & List Main Area End -->
                        </div>

                    </div>
                    <!-- product Categorie List End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Shop Page End -->



@endsection

@section('scripts')
<script>

  $(document).ready(function() {
    var categories = [];
    @php
    $caties = [];
    if(isset($_GET['categories'])){
     $caties = [];
     $caties = explode(',', $_GET['categories']);     
    }
    @endphp
    categories = @php echo json_encode($caties); @endphp;
    addToPagination();
    // when dynamic attribute changes
    $(".attribute-input, #sortby").on('change', function() {
      $("#ajaxLoader").show();
      filter();
    });

    $('input[name="cat[]"]').on('change', function (e) {
        e.preventDefault();
        categories = [];
        $('input[name="cat[]"]:checked').each(function(){
            categories.push($(this).val());
        });
        $("#ajaxLoader").show();
        filter();
    });

    // when price changed & clicked in search button
    $(".filter-btn").on('click', function(e) {
      e.preventDefault();
      $("#ajaxLoader").show();
      filter();
    });

  function filter() {
    let filterlink = '';

    if ($("#prod_name").val() != '' && $("#prod_name").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?search='+$("#prod_name").val();
      } else {
        filterlink += '&search='+$("#prod_name").val();
      }
    }

    if (categories.length > 0) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+'categories='+categories;
      } else {
        filterlink += '&categories='+categories;
      }
    }

    $(".attribute-input").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });

    if ($("#sortby").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#sortby").attr('name')+'='+$("#sortby").val();
      } else {
        filterlink += '&'+$("#sortby").attr('name')+'='+$("#sortby").val();
      }
    }

    if ($("#minamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#minamount").attr('name')+'='+$("#minamount").val();
      } else {
        filterlink += '&'+$("#minamount").attr('name')+'='+$("#minamount").val();
      }
    }

    if ($("#maxamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      } else {
        filterlink += '&'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      }
    }

    // console.log(filterlink);
    console.log(encodeURI(filterlink));
    $("#ajaxContent").load(encodeURI(filterlink), function(data) {
      // add query string to pagination
      addToPagination();
      $("#ajaxLoader").fadeOut(1000);
      $('#show-total-pp').html($('#total-pp').val());  
    });
  }

  // append parameters to pagination links
  function addToPagination() {
    // add to attributes in pagination links
    $('.pagination-url').each(function() {
      let url = $(this).attr('href');
      let queryString = '?' + url.split('?')[1]; // "?page=1234...."

      let urlParams = new URLSearchParams(queryString);
      let page = urlParams.get('page'); // value of 'page' parameter
      let fullUrl = '{{route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')])}}?page='+page+'&search='+'{{request()->input('search')}}';

      $(".attribute-input").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      if ($("#sortby").val() != '') {
        fullUrl += '&sort='+encodeURI($("#sortby").val());
      }

      if ($("#minamount").val() != '') {
        fullUrl += '&min='+encodeURI($("#minamount").val());
      }

      if ($("#maxamount").val() != '') {
        fullUrl += '&max='+encodeURI($("#maxamount").val());
      }

    if (categories.length > 0) {
        fullUrl += '&categories='+categories;
    }

      $(this).attr('href', fullUrl);
    });
  }

  $(document).on('click', '.categori-item-area .pagination li a', function (event) {
    event.preventDefault();
    if ($(this).attr('href') != '#' && $(this).attr('href')) {
      $('#preloader').show();
      $('#ajaxContent').load($(this).attr('href'), function (response, status, xhr) {
        if (status == "success") {
          $('#preloader').fadeOut();
          $("html,body").animate({
            scrollTop: 0
          }, 1);

          addToPagination();
        }
      });
    }
  });
});

</script>
@endsection