@extends('layouts.front')
@section('title' , 'Home')
@section('styles')
    <style>
        .white{
            color:#c7b270;
        }
    </style>
@endsection
@section('content')
                   <!-- Main Header Area End Here -->
        <!-- Slider Area Start -->
        <div class="slider-area">
            <!-- Slider Area Start Here -->
            <div class="slider-activation owl-carousel">
                <!-- Start Single Slide -->
                @foreach($sliders as $slider)
                <div style="background-image: url({{asset('assets/images/sliders/'.$slider->photo)}});background-repeat: no-repeat;background-position: center center;background-size: cover;" class="slide align-center-left fullscreen animation-style-01">
                    <div class="slider-progress"></div>
                    <div class="container"> 
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content">
                                    <h1 style="font-size:{{$slider->title_size}}px !important; color:{{$slider->title_color}} !important; {{$slider->title_anime}}">{{ $slider->title_text }}</h1>
                                    <h2 style="font-size:{{$slider->subtitle_size}}px !important; color:{{$slider->subtitle_color}} !important; {{$slider->subtitle_anime}}">{{$slider->subtitle_text}}</h2>
                                    <p style="font-size:{{$slider->details_size}}px !important; color:{{$slider->details_color}} !important; {{$slider->details_anime}}">{{$slider->details_text}}</p>
                                    <div class="slide-btn putty-color">
                                        <a href="{{$slider->link}}">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- End Single Slide -->
            </div>
            <!-- Slider Area End Here -->
        </div>
        <!-- Slider Area End -->
        <!--  Top Banner Area Start -->
        <div class="banner-area pt-20 pb-90">
            <div class="container">
                <div class="row">
                    <!--  Single Banner Area Start -->
                    <div class="col-lg-4 col-md-4 mb-sm-30">
                        <div class="single-banner zoom">
                            <a target="_blank" href="{{ url($gs->home_fp_shop_now) }}">
                                <img src="{{ asset('public/assets/images/home/'.$gs->home_fp_image)}}" alt="banner-img">
                            </a>
                        </div>
                    </div>
                    <!--  Single Banner Area End -->
                    <!--  Single Banner Area Start -->
                    <div class="col-lg-4 col-md-4 mb-sm-30">
                        <div class="single-banner zoom">
                        <a target="_blank" href="{{ url($gs->home_sp_shop_now) }}">
                            <img src="{{ asset('public/assets/images/home/'.$gs->home_sp_image)}}" alt="banner-img">
                            </a>
                        </div>
                    </div>
                    <!--  Single Banner Area End -->
                    <!--  Single Banner Area Start -->
                    <div class="col-lg-4 col-md-4">
                        <div class="single-banner zoom">
                        <a target="_blank" href="{{ url($gs->home_tp_shop_now) }}">
                                <img src="{{ asset('public/assets/images/home/'.$gs->home_tp_image)}}" alt="banner-img">
                            </a>
                        </div>
                    </div>
                    <!--  Single Banner Area End -->
                </div>
            </div>
        </div>
        <!--  Top Banner Area End -->
        <!--  Deal Product Start Here -->
        <div class="deal-pro bg-image-18">
            <div class="container">
                <div class="row align-items-center">
                    <div class="offset-lg-6 col-lg-6">
                        <div class="main-deal-pro">
                            @if(count($offers) > 0)
                            <div class="section-title deal-header">
                                <h2>{{ $gs->home_offers_heading }}</h2>
                                <p>{!! $gs->home_offers_text !!}</p>
                            </div>
                            <div class="daily-deal-active owl-carousel">
                                <!-- Single Product Start Here -->
                                @foreach($offers as $offer)
                                <div class="single-makal-product">
                                    <div class="countdown" data-countdown="{{Carbon\Carbon::parse($offer->end_date)->format('Y/m/d')}}"></div>
                                    <div class="pro-img offrs">
                                        <a data-toggle="modal" data-target="#offerquickview" data-href="{{ route('offer.quick',$offer->id) }}" class="quick-view offer-quick-a" href="#">
                                            <img src="{{ asset('public/assets/images/offers/'.$offer->image)}}" alt="product-img">
                                        </a>
                                        <span class="sticker-new">new</span>
                                        <div class="quick-view-pro">
                                            <a data-toggle="modal" data-target="#offerquickview" data-href="{{ route('offer.quick',$offer->id) }}" class="quick-view offer-quick-a" href="#"></a>
                                        </div>
                                    </div>
                                    <div class="pro-content">
                                        <h4 class="pro-title">
                                            <a href="product-details.html">{{ $offer->name }}</a>
                                        </h4>
                                        <p>
                                            <span class="price">{{ $offer->showPrice() }}</span>
                                        </p>
                                        <div class="pro-actions">
                                            <div class="actions-primary">
                                                <a href="javascript:;" data-href="{{ route('product.offer.cart.add',$offer->id) }}" class="add-offer-to-cart" data-toggle="tooltip" data-original-title="Add to Cart">Add To Cart</a>
                                            </div>
                                            <!-- <div class="actions-secondary">
                                                <div class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <div class="row pt-5 pb-5" style="background:white; border-radius:5px;">
                                    <div class="col-md-8 mx-auto">
                                        <h5 class="mb-2 white">subscribe for alerts of latest and best deals.</h5>
                                    </div>
                                    <div class="col-md-8 mx-auto">
                                        <form id="subscribeform" action="{{ route('front.subscribe') }}" method="post">
                                        @csrf
                                            <input type="text" class="form-control" name="email" placeholder="Enter Email Address" required />
                                            <button style="background:#c7b270; color:white;" class="mt-2 btn btn-default btn-block" type="submit" class="pe-7s-mail-open">Subscribe</button>
                                        </form>                                    
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  Deal Product End Here -->
        @if(count($feature_products)>0)
        <!-- New Arrival Products Start Here -->
        <div class="new-arrival no-border-style ptb-90">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section-title text-center">
                    <h2>{{$gs->home_featured_heading}}</h2>
                    <p>{!! $gs->home_featured_text !!}</p>
                </div>
                <!-- Section Title End -->
                <div class="our-pro-active owl-carousel">
                    <!-- New Arrival Dual Products Start Here -->

                        @foreach($feature_products as $product)
                        <!-- Single Product Start Here -->
                        <div class="single-makal-product">
                        <a style="position:absolute; right:30px; top:20px; z-index:999;" id="{{$product->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$product->id) }}"><i class="wish_{{ $product->id }} @if($wishs->contains('product_id',$product->id)) fa fa-heart @else fa fa-heart-o @endif"></i></a>
                            <div class="pro-img prods">
                                <a href="{{ route('front.product',$product->slug) }}">
                                    <img src="{{ asset('public/assets/images/products/'.$product->photo) }}" alt="{{ $product->name }}">
                                </a>
                                @if($product->dsc)
                                <span class="sticker-new">-{{ $product->dsc }}%</span>
                                @endif
                                <div class="quick-view-pro">
                                    <a data-toggle="modal" href="javascript:;" title="Quick view" data-href="{{ route('product.quick',$product->id) }}" data-target="#quickview" class="wish-view quick-a"></a>
                                </div>
                                <div class="quick-view-pro">
                                    <a data-toggle="modal" href="javascript:;" title="Quick view" data-href="{{ route('product.quick',$product->id) }}" data-target="#quickview" class="quick-view quick-a"></a>
                                </div>
                            </div>
                            <div class="pro-content">
                                <h4 class="pro-title">
                                    <a href="{{ route('front.product',$product->slug) }}">{{ $product->name }}</a>
                                </h4>
                                <p>
                                    <span class="price">{{ $product->showPrice() }}</span>
                                </p>
                                <div class="pro-actions">
                                    <div class="actions-primary">
                                        <a data-href="{{ route('product.cart.add',$product->id) }}" class="cart-btn add-to-cart" data-toggle="tooltip" data-original-title="Add to Cart">Add To Cart</a>
                                    </div>
                                    <div class="actions-secondary">
                                        {!! $product->showRating() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Single Product End Here -->
                        @endforeach

                    <!-- New Arrival Dual Products End Here -->
                </div>
            </div>
        </div>
        @endif
        <!-- New Arrival Products End Here -->
        <!-- New Product Banner Start Here -->
        <!-- <div class="product-bannner pro-border-style">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 mb-sm-30">
                        <div class="single-banner">
                            <a href="shop.html">
                                <img src="{{ asset('public/assets/front/img/banner/cosmetic/1-4.jpg')}}" alt="banner-img">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-banner">
                            <a href="shop.html">
                                <img src="{{ asset('public/assets/front/img/banner/cosmetic/1-5.jpg')}}" alt="banner-img">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- New Product Banner End Here -->
        @if(count($ratings) > 0)
        <!-- Testmonial Start Here -->
        <div class="testmonial bg-image-5 ptb-90">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section-title text-center cl-testmonial">
                    <h2>{{ $gs->home_testimonials_heading }}</h2>
                    <p>{!! $gs->home_testimonials_text !!}</p>
                </div>
                <!-- Section Title End -->
                <div class="testmonial-active owl-carousel">
                    <!-- Single Slide Testmonial Start -->
                    @foreach($ratings as $rating)                    
                    <div class="single-testmonial text-center">
                        <div class="testmonial-content">
                            <p>{!! $rating->review !!}</p>
                            <img src="{{ asset('public/assets/front/img/testmonial/t2.png')}}" alt="testmonial-img">
                            <span class="t-author">{{ $rating->user->name }}</span>
                        </div>
                    </div>
                    @endforeach
                    <!-- Single Slide Testmonial End -->
                </div>
            </div>
        </div>
        <!-- Testmonial End Here -->
        @endif
        <!-- Support Area Start Here -->
        <div class="support-area pt-90 pb-90">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 mb-all-30">
                            <div class="single-support">
                                <div class="pe-7s-plane icon"></div>
                                <div class="support-desc">
                                    <h6>Free Shipping</h6>
                                    <span>Free shipping on all US order or order above $200</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 mb-all-30">
                            <div class="single-support">
                                <div class="pe-7s-help2 icon"></div>
                                <div class="support-desc">
                                    <h6>Support 24/7</h6>
                                    <span>Contact us 24 hours a day, 7 days a week</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 mb-xsm-30">
                            <div class="single-support">
                                <div class="pe-7s-back icon"></div>
                                <div class="support-desc">
                                    <h6>30 Days Return</h6>
                                    <span>Simply return it within 30 days for an exchange</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-support">
                                <div class="pe-7s-credit icon"></div>
                                <div class="support-desc">
                                    <h6>100% Payment Secure</h6>
                                    <span>We ensure secure payment with PEV</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
            </div>
            <!-- Container End -->
        </div>
        <!-- Support Area End Here -->

@endsection
@section('scripts')
@endsection