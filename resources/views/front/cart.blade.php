@extends('layouts.front')
@section('styles')
<style>
    .coupon-btn{
        background:#c7b270;
        color:white;
    }
      @media only screen and (max-width: 600px) {
            .qtybtns {
                padding:0px 3px;
            }
            .cart-block{
                margin-top:10%;
            }
            .cart-block table{
                width:100%;
            }
            .cart-block table th{
                text-align:left;
            }
            .cart-block .wc-proceed-to-checkout a{
                width:100%;
                text-align:center;
            }
      }
</style>
@endsection
@section('content')
            <!-- Breadcrumb Area Start Here -->
            <div class="breadcrumb-area">
                <div class="container">
                    <ol class="breadcrumb breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                        <li class="breadcrumb-item active">
                            Cart
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area End -->

        <!-- Cart Main Area Start -->
        <div class="cart-main-area ptb-90">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- Form Start -->
    <form action="#">
        <!-- Table Content Start -->
        @php
            $discount = 0;
        @endphp
        @if(Session::has('cart') && Session::get('cart')->items)
        <h5 class="mb-2">Products</h5>
        <div class="table-content table-responsive mb-45">
            <table>
            @include('includes.form-success')
                <thead>
                    <tr>
                        <th class="product-thumbnail">Image</th>
                        <th class="product-name">Product</th>
                        <th class="product-price">Price</th>
                        <th class="product-quantity">Quantity</th>
                        <th class="product-subtotal">Total</th>
                        <th class="product-remove">Remove</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                @php
                    $discount = $discount + (App\Models\Product::find($product['item']['id'])->dsc_amt * $product['qty']);
                @endphp
                <tr class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values']) }}">
                        <td class="product-thumbnail">
                            <a href="#">
                                <img src="{{ $product['item']['photo'] ? asset('public/assets/images/products/'.$product['item']['photo']):asset('public/assets/images/noimage.png') }}" alt="cart-image" />
                            </a>
                        </td>
                        <td class="product-name">                        
                        <a href="{{ route('front.product', $product['item']['slug']) }}">{{mb_strlen($product['item']['name'],'utf-8') > 35 ? mb_substr($product['item']['name'],0,35,'utf-8').'...' : $product['item']['name']}}</a>
                        <br>                                     
                            @if(!empty($product['size']))
                            <b>{{ $langg->lang312 }}</b>: {{ $product['item']['measure'] }}{{str_replace('-',' ',$product['size'])}} <br>
                            @endif
                            @if(!empty($product['color']))
                            <div class="mt-1 mb-1">
                            <b>{{ $langg->lang313 }}</b>:   &nbsp;<span id="color-bar" style="padding-right:20px; border: 5px solid #{{$product['color'] == "" ? "white" : $product['color']}}; background:#{{$product['color'] == "" ? "white" : $product['color']}};"></span>
                            </div>
                            @endif

                            @if(!empty($product['keys']))
                            @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)

                                <b>{{ ucwords(str_replace('_', ' ', $key))  }} : </b> {{ $value }} <br>
                            @endforeach
                            @endif
                        </td>
                        <td class="product-price">
                            <span class="amount">{{ App\Models\Product::convertPrice($product['item']['price']) }}</span>
                        </td>
                        <td class="product-quantity">                                            
                            @if($product['item']['type'] == 'Physical')
                            <input type="hidden" class="prodid" value="{{$product['item']['id']}}">  
                            <input type="hidden" class="itemid" value="{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])}}">     
                            <input type="hidden" class="size_qty" value="{{$product['size_qty']}}">     
                            <input type="hidden" class="size_price" value="{{$product['item']['price']}}">   
                            <div class="plus-minus">
                                    <div class="dec qtybutton reducing" style="display: inline-block;">
                                        <span class="qtybtns btn btn-default"><i class="fa fa-minus"></i></span>                                    
                                    </div>
                                    <span class="qttotal1" id="qty{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.') ,'',$product['values'])}}">{{ $product['qty'] }}</span>
                                    <div class="inc qtybutton adding" style="display: inline-block;">
                                        <span class="qtybtns btn btn-default"><i class="fa fa-plus"></i></span>
                                    </div>
                            </div>
                            @endif
                        </td>
                        <td class="product-subtotal">
                            @if($product['size_qty'])
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])}}" value="{{$product['size_qty']}}">
                            @elseif($product['item']['type'] != 'Physical') 
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])}}" value="1">
                            @else
                            <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])}}" value="{{$product['stock']}}">
                            @endif

                            <span id="prc{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])}}">
                                {{ App\Models\Product::convertPrice($product['price']) }}                 
                            </span>
                        </td>
                        <td class="product-remove">                
                            <span class="removecart cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,.'),'',$product['values'])) }}"><i class="fa fa-times"></i></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif


    @if(Session::has('cart') && Session::get('cart')->offers)
        <h5 class="mb-2">Offers</h5>
        <div class="table-content table-responsive mb-45">
            <table>
                <thead>
                    <tr>
                        <th class="product-thumbnail">Image</th>
                        <th class="product-name">Offer</th>
                        <th class="product-price">Price</th>
                        <th class="product-quantity">Quantity</th>
                        <th class="product-subtotal">Total</th>
                        <th class="product-remove">Remove</th>
                    </tr>
                </thead>
                <tbody>
                @foreach(Session::get('cart')->offers as $offer)
                <tr class="oremove{{$offer['id']}}">
                    @php 
                        $offr = \App\Models\Offer::query()->where("id",$offer['id'])->first();
                    @endphp                        

                    <td class="product-thumbnail">
                        <a href="#">
                            <img src="{{ asset('public/assets/images/products').'/'.$offr->product->photo }}" alt="cart-image" />
                        </a>
                    </td>
                    <td class="product-name">
                        <a href="">{{ $offr->name }}</a><br>
                        <small><b>Offer Type</b> : @if($offr->type == 0) Buy One/Get One @elseif($offr->type == 1) Special Discount @else Special Quantity @endif</small><br>
                        @if($offr->type == 0)
                        <small><b>Buying Item</b> : {{$offr->product->name}}</small><br>
                        <small><b>Free Item</b> : {{$offr->free->name}}</small>
                        @endif
                        @if($offr->type == 1)
                        <small><b>Original Price</b> : {{ $offr->product->showPrice() }}</small><br>
                        <small><b>Special Discount</b> : {{ $offr->discount }}%</small><br>
                        @endif
                        @if($offr->type == 2)
                        <small><b>Product</b> : {{ $offr->product->name }}</small><br>
                        <small><b>Original Price</b> : {{ $offr->product->showPrice() }}</small><br>
                        <small><b>Discount</b> : {{ $offr->discount_q }}%</small><br>
                        <small><b>Quantity</b> : {{ $offr->quantity }}</small><br>
                        @endif
                    <br>                                     

                    </td>
                    <td class="product-price">
                        <span class="amount">{{ App\Models\Product::convertPrice($offer['price']) }}</span>
                    </td>
                    <td class="product-quantity">                                            
                        {{ $offer['qty'] }}
                    </td>
                    <td class="product-subtotal">
                        <span id="offr{{$offer['id']}}">
                            {{ App\Models\Product::convertPrice($offer['price']) }}                 
                        </span>
                        </td>
                        <td class="product-remove">                
                            <span class="offerremovecart cart-offer-remove" data-class="oremove{{ $offer['id'] }}" data-href="{{ route('offer.cart.remove',$offer['id']) }}"><i class="fa fa-times"></i></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif

        <!-- Table Content Start -->
        <div class="row">
            <!-- Cart Button Start -->
            <div class="col-md-8 col-sm-12">
                <div class="buttons-cart">
                    <!-- <input type="submit" value="Update Cart" /> -->
                    <a href="{{ url('/') }}">Continue Shopping</a>                    
                </div>

                <div class="row">
                <div class="col-md-12 col-sm-12">
                <div class="cart_totals">
                    <h2 class="mb-2">Discount</h2>
                </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                    <div class="discount-coupon mt-3">
                        <form id="coupon-form" class="coupon">
                            <!-- <input type="hidden" class="coupon-total" id="grandtotal" value="{{ Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00' }}">
                            <input type="hidden" id="coupon_val" value="0"> -->
                            <div class="row">
                                <div class="col-9 pr-0">
                                    <input type="text" placeholder="Enter your codes" id="code" class="form-control" required="" autocomplete="off">
                                </div>
                                <div class="col-3 pl-0">
                                    <button type="submit" class="coupon-submit site-btn btn btn-default coupon-btn">Apply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>


                </div>
            </div>


            <!-- Cart Button Start -->
            <!-- Cart Totals Start -->
            <div class="col-md-4 col-sm-12 cart-block">
                <div class="cart_totals float-md-right text-md-right">
                    <h2>Cart Total</h2>
                    <br />
                    <table class="float-md-right">
                        <tbody>


                    <input type="hidden" id="d-val" value="{{ App\Models\Product::convertPrice($discount)}}">
                    <input type="hidden" id="c-val" value="{{ $curr->sign }}">
                    <input type="hidden" id="c-sign-length" value="{{ mb_strlen($curr->sign) }}">

                    <input type="hidden" class="coupon-total" id="grandtotal" value="{{ Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00' }}">
                    <input type="hidden" id="coupon_val" value="0">

                        <tr class="cart-subtotal">
                            <th>Subtotal</th>
                            <td><span class="amount cart-total">{{ Session::has('cart') ? App\Models\Product::convertPrice($totalPrice) : '0.00' }}</span></td>
                        </tr>
                        <tr class="cart-subtotal">
                            <th>Tax <small>({{$gs->tax}}%)</small></th>
                            @php 
                            if($gs->tax != 0)
                                {
                                    $tax = ($totalPrice / 100) * $gs->tax;
                                    $mainTotal = $totalPrice + $tax;
                                }
                            @endphp
                            <td><span id="tax-total" class="amount">{{ App\Models\Product::convertPrice($tax)}}</span></td>
                        </tr>
                        <tr class="cart-subtotal">
                            <th>Discount </th>
                            <td><span id="in_discount" class="amount">{{ App\Models\Product::convertPrice($discount)}}</span></td>
                        </tr>
                        <tr class="cart-subtotal">
                            <th>Discount <small>(Coupon)</small></th>
                            <td><span class="amount discount">{{ $curr->sign }} 0</span></td>
                        </tr>
                        <tr class="order-total">
                            <th>Total</th>
                            <td>
                                <strong><span class="amount main-total">@if(Session::has('cart')) {{$curr->sign}} @endif {{ Session::has('cart') ?  App\Models\Product::convertOnlyPrice($mainTotal) - App\Models\Product::convertOnlyPrice($discount)  : '0.00' }}</span></strong>                                    
                            </td>
                        </tr>
                    </tbody>
                </table>
                    <div class="wc-proceed-to-checkout">
                        <a href="{{ route('front.checkout') }}">Proceed to Checkout</a>
                    </div>
                </div>
            </div>
            <!-- Cart Totals End -->
        </div>
        <!-- Row End -->
    </form>
    <!-- Form End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
        </div>
        <!-- Cart Main Area End -->
        @endsection

@section('scripts')
    <script>

    </script>
@endsection