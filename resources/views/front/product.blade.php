@extends('layouts.front')
@section('title' , 'Product')

@section('styles')
    <style>
        .custom-control {
            position: relative;
            display: inline-block;
            margin-left:10px;
            min-height: 1.5rem;
            padding-left: 1.5rem;
            padding-top:2px;
        }
        .custom-radio .custom-control-input:checked~.custom-control-label::before {
            background-color: #c7b270;
        }
        .single-makal-product:hover .wish-view::before {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }
        .template-color-1 .wish-view:hover::before {
            background: #c7b270;
            color: #fff;
        }
        .wish-view::before {
            content: "";
            font-family: "Pe-icon-7-stroke";
            font-size: 18px;
        }
        .wish-view::before {
            background: #fff none repeat scroll 0 0;
            border-radius: 100%;
            color: #a3a3a3;
            content: "î˜˜";
            display: block;
            font-size: 20px;
            height: 44px;
            line-height: 44px;
            width: 44px;
            opacity: 0;
            position: absolute;
            right: 15px;
            bottom: 30px;
            text-align: center;
            -webkit-transform: scale(0.6);
            transform: scale(0.6);
            z-index: 99;
        }


        /* ratings Csss */

.rating-product{
    width:100%;
}
.rating { 
  border: none;
  margin:0px;
  margin-bottom: 0px;
  float: left;
}

.rating > input { display: none; } 

.rating.star > label {
    color: #78e2fb;
    margin: 1px 0px 0px 0px;
    background-color: #ffffff;
    border-radius: 0;
    height: 40px;
    float: right;
    width: 36px;
    border: 1px solid #ffffff;
}
fieldset.rating.star > label:before { 
    margin-top: 0;
    padding: 0px;
    font-size: 30px;
    font-family: FontAwesome;
    display: inline-block;
    content: "\2605";
    position: relative;
    top: -9px;
}
.rating > label:before {
    margin-top: 2px;
    padding: 5px 12px;
    font-size: 1.25em;
    font-family: FontAwesome;
    display: inline-block;
    content: "";
}
.rating > .half:before { 
  content: "\f089";
  position: absolute;
}
.rating.star > label{
  background-color: transparent !important;
}
.rating > label { 
    color: #fff;
    margin: 1px 11px 0px 0px;
    background-color: #78e2fb;
    border-radius: 2px;
    height: 16px;
    float: right;
    width: 16px;
    border: 1px solid #c1c0c0;  
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { 
	background-color:#c7b270!important;
  cursor:pointer;
} /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { 
	background-color:#c7b270!important;
  cursor:pointer;
} 
.rating.star:not(:checked) > label:hover, /* hover current star */
.rating.star:not(:checked) > label:hover ~ label { 
  color:#c7b270!important;
  background-color: transparent !important;
  cursor:pointer;
} /* hover previous stars in list */

.rating.star > input:checked + label:hover, /* hover current star when changing rating.star */
.rating.star > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating.star > input:checked ~ label:hover ~ label { 
  color:#c7b270!important;
  cursor:pointer;
  background-color: transparent !important;
} 
.rating.star {
    margin-left: 5%;
}

.review-mini-title {
    color: #292929;
    font-size: 18px;
    font-weight: 500;
    margin: 20px 0 10px 0;
    text-transform: capitalize;
}
    </style>
@endsection

@section('content')

   <!-- Breadcrumb Area Start Here -->
   <div class="breadcrumb-area">
            <div class="container">
                <ol class="breadcrumb breadcrumb-list">
                    <li class="breadcrumb-item"><a href="index-cosmetic.html">Home</a></li>

                    <li class="breadcrumb-item">
                        <a href="{{route('front.category',$productt->category->slug)}}">{{$productt->category->name}}</a>
                    </li>
                    @if($productt->subcategory_id != null)
                    <li class="breadcrumb-item">
                        <a href="{{ route('front.subcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug]) }}">{{$productt->subcategory->name}}</a>
                    </li>                        
                    @endif
                    @if($productt->childcategory_id != null)
                    <li class="breadcrumb-item"><a
                        href="{{ route('front.childcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug, 'slug3' => $productt->childcategory->slug]) }}">{{$productt->childcategory->name}}</a></li>
                    @endif
                    <li class="breadcrumb-item active">
                        {{ $productt->name }}
                    </li>
                </ol>
            </div>
        </div>
        <!-- Breadcrumb Area End Here -->




        <!-- Product Thumbnail Start -->
        <div class="main-product-thumbnail white-bg ptb-90">
            <div class="container">
                <div class="row">
                    <!-- Main Thumbnail Image Start -->
                    <div class="col-lg-4 col-md-6 mb-all-40">
                        <!-- Thumbnail Large Image start -->
                        
                        <div class="tab-content">
                            <div id="product{{$productt->id}}" class="zoom-image tab-pane fade show active">
                                <a data-fancybox="images" href="{{asset('public/assets/images/products/'.$productt->photo)}}">
                                    <img data-original-image="{{asset('public/assets/images/products/'.$productt->photo)}}" src="{{asset('public/assets/images/products/'.$productt->photo)}}" alt="product-view">                                
                                </a>
                            </div>
                            @foreach($productt->galleries as $gal)
                            <div id="gal{{$gal->id}}" class="zoom-image tab-pane fade">
                                <a data-fancybox="images" href="{{asset('public/assets/images/galleries/'.$gal->photo)}}">
                                    <img data-original-image="{{asset('public/assets/images/galleries/'.$gal->photo)}}" src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="product-view">
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <!-- Thumbnail Large Image End -->
                        <!-- Thumbnail Image End -->
                        <div class="product-thumbnail">
                            <div class="thumb-menu owl-carousel nav tabs-area" role="tablist">
                            <a class="active" data-toggle="tab" href="#product{{$productt->id}}"><img src="{{asset('public/assets/images/products/'.$productt->photo)}}" alt="product-thumbnail"></a>
                            @foreach($productt->galleries as $gal)
                                <a data-toggle="tab" href="#gal{{$gal->id}}"><img src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" alt="product-thumbnail"></a>
                            @endforeach                            
                            </div>
                        </div>
                        <!-- Thumbnail image end -->
                    </div>
                    <!-- Main Thumbnail Image End -->
                    <!-- Thumbnail Description Start -->
                    <div class="col-lg-8 col-md-6">
                        <div class="thubnail-desc fix">
                            <h3 class="product-header">{{ $productt->name }}</h3>
                            {!! $productt->showRating() !!}
                            <ul style="margin-left: 8%;" class="rating-summary">
                                <li class="read-review"><a href="#review">reviews ({{count($productt->ratings)}})</a></li>
                            </ul>
                            <div class="pro-thumb-price mt-10">
                                <p class="d-flex align-items-center">
                                <!-- <span class="prev-price">16.51</span> -->
                                <span id="sizeprice" class="price">{{ $productt->showPrice() }}</span>
                                        @if($productt->dsc)
                                        <span class="saving-price">-{{$productt->dsc}}%</span>
                                        @endif
                                </p>
                            </div>

                            <input type="hidden" id="product_price" value="{{ round($productt->vendorPrice() * $curr->value,2) }}">
                            <input type="hidden" id="product_id" value="{{ $productt->id }}">
                            <input type="hidden" id="curr_pos" value="{{ $gs->currency_format }}">
                            <input type="hidden" id="curr_sign" value="{{ $curr->sign }}">

                            <div class="pro-desc-details">
                                {!! $productt->details !!}
                            </div>

                            <div class="info-meta-3">
                    <ul class="meta-list">
                      @if (!empty($productt->attributes))
                        @php
                          $attrArr = json_decode($productt->attributes, true);
                        @endphp
                      @endif
                      @if (!empty($attrArr))
                        <div class="product-attributes my-4">
                          <div class="row">
                          @foreach ($attrArr as $attrKey => $attrVal)
                            @if (array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1)

                          <div class="col-lg-6">
                              <div class="form-group mb-2">
                                <strong for="" class="text-capitalize">{{ str_replace("_", " ", $attrKey) }} :</strong>
                                <div class="mt-3">
                                @foreach ($attrVal['values'] as $optionKey => $optionVal)
                                  <div class="custom-control custom-radio">
                                    <input type="hidden" class="keys" value="">
                                    <input type="hidden" class="values" value="">
                                    <input type="radio" id="{{$attrKey}}{{ $optionKey }}" name="{{ $attrKey }}" class="custom-control-input product-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" value="{{ $optionVal }}" {{ $loop->first ? 'checked' : '' }}>
                                    <label class="custom-control-label radio-inline" for="{{$attrKey}}{{ $optionKey }}">{{ $optionVal }}

                                    @if (!empty($attrVal['prices'][$optionKey]))
                                      +
                                      {{$curr->sign}} {{$attrVal['prices'][$optionKey] * $curr->value}}
                                    @endif
                                    </label>
                                  </div>
                                @endforeach
                                </div>
                              </div>
                          </div>
                            @endif
                          @endforeach
                          </div>
                        </div>
                      @endif

                      @if($productt->emptyStock())
                      <li class="addtocart">
                        <a href="javascript:;" class="cart-out-of-stock">
                          <i class="icofont-close-circled"></i>
                          {{ $langg->lang78 }}</a>
                      </li>
                      @endif



                    </ul>
                  </div>

                            <!-- <div class="product-size mtb-30 clearfix">
                                <label>Size</label>
                                <select class="">
                                    <option>S</option>
                                    <option>M</option>
                                    <option>L</option>
                                </select>
                            </div>
                            <div class="color clearfix mb-30">
                                <label>color</label>
                                <ul class="color-list">
                                    <li>
                                        <a class="white" href="#"></a>
                                    </li>
                                    <li>
                                        <a class="orange active" href="#"></a>
                                    </li>
                                    <li>
                                        <a class="paste" href="#"></a>
                                    </li>
                                </ul>
                            </div> -->
                            <div class="mt-4 quatity-stock">
                                <label>Quantity</label>
                                <ul class="d-flex flex-wrap  align-items-center">
                                    <li class="box-quantity">
                                        <form action="#">
                                            <input class="quantity qttotal" type="number" min="1" value="1">
                                        </form>
                                    </li>
                                    <li>
                                        <button href="javascript:;" id="addcrt" class="pro-cart">add to cart</button>
                                    </li>
                                    <li class="pro-ref">
                                        <p><span class="in-stock"><i class="ion-checkmark-round"></i> 
                                        
                                        @if($productt->stock != null)
                                            {{ $productt->stock}} items(s) In Stock
                                        @endif

                                        </span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="social-sharing mt-30">
                                <ul>
                                    <li><label>share</label></li>

                                    @php $url = route('front.product',$productt->slug); @endphp
                                    <li><a target="_blank" href="http://www.facebook.com/sharer.php?u={{$url}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="http://twitter.com/share?url={{$url}}&text=Simple Share Buttons&hashtags=simplesharebuttons"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://plus.google.com/share?url={{$url}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{$url}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="mailto:?Subject=Simple Share Buttons&Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{$url}}"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Thumbnail Description End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Product Thumbnail End -->
        <!-- Product Thumbnail Description Start -->
        <div class="thumnail-desc">
            <div class="container">
                <div class="thumb-desc-inner">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="main-thumb-desc nav tabs-area" role="tablist">
                                <li><a class="active" data-toggle="tab" href="#dtail">Description</a></li>
                                <li><a data-toggle="tab" href="#review">Reviews ({{count($productt->ratings)}})</a></li>
                            </ul>
                            <!-- Product Thumbnail Tab Content Start -->
                            <div class="tab-content thumb-content">
                                <div id="dtail" class="tab-pane fade show active">
                                    <p>
                                        {!! $productt->details !!}
                                    </p>
                                </div>
                                <div id="review" class="tab-pane fade">
                                    <!-- Reviews Start -->
                                    <div class="review">
                                        <div class="group-title">
                                            <h2>customer review</h2>
                                        </div>
                                        @foreach($productt->ratings as $rating)
                                        <h4 class="review-mini-title">{{ $rating->user->name }}</h4>
                                        <ul class="review-list">
                                            <!-- Single Review List Start -->
                                            <li>
                                                <span>Rating</span>
                                                @php
                                                $i = 0;
                                                for($i=0;$i<5;$i++){
                                                   if($i<$rating->rating){
                                                    echo '<i class="fa fa-star"></i>';                                                       
                                                   }else{
                                                    echo '<i class="fa fa-star-o"></i>';                                                       
                                                   }     
                                                }
                                                @endphp
                                                <p>{{ $rating->review }}</p>
                                            </li>
                                            <!-- Single Review List End -->
                                        </ul>
                                        @endforeach
                                    </div>
                                    <!-- Reviews End -->
                                    <!-- Reviews Start -->
                                    <div class="review mt-10">
                                        <h2 class="review-title mb-30">You're reviewing: <br><span>{{ $productt->name }}</span></h2>

                                        <!-- Reviews Field Start -->
                                        <div class="riview-field mt-40">
                                        @if(Auth::guard('web')->check())
                                        <form id="reviewform" action="{{route('front.review.submit')}}"
                              data-href="{{ route('front.reviews',$productt->id) }}" method="POST">
                              @include('includes.admin.form-both')
                              {{ csrf_field() }}
                    
                    <input type="hidden" id="rating" name="rating" value="5">
                    <input type="hidden" name="user_id" value="{{Auth::guard('web')->user()->id}}">
                    <input type="hidden" name="product_id" value="{{$productt->id}}">

                    <div class="star-box">
                            <div class="row rating-product">
                                <div class="col-md-2"><span>Your Rating:</span></div>
                                <div class="col-md-8">
                                    <div class="">
                                        <fieldset class="rating star">
                                            <input type="radio" id="field6_star5" name="rating" value="5" /><label class = "rating full" id="5" for="field6_star5"></label>
                                            <input type="radio" id="field6_star4" name="rating" value="4" /><label class = "rating full" id="4" for="field6_star4"></label>
                                            <input type="radio" id="field6_star3" name="rating" value="3" /><label class = "rating full" id="3" for="field6_star3"></label>
                                            <input type="radio" id="field6_star2" name="rating" value="2" /><label class = "rating full" id="2" for="field6_star2"></label>
                                            <input type="radio" id="field6_star1" name="rating" value="1" /><label class = "rating full" id="1" for="field6_star1"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>

                        </div>


                                                <div class="form-group">
                                                    <label class="req" for="comments">Your Review</label>
                                                    <textarea class="form-control" name="review" placeholder="{{ $langg->lang99 }}" rows="5" id="comments" required="required"></textarea>
                                                </div>
                                                <button type="submit" class="customer-btn">Submit</button>
                                            </form>
                                            @else
                                            <a href="{{ route('user.login') }}"><button class="btn btn-default">Login to Submit Review</button></a>
                                            @endif
                                        </div>
                                        <!-- Reviews Field Start -->
                                    </div>
                                    <!-- Reviews End -->
                                </div>
                            </div>
                            <!-- Product Thumbnail Tab Content End -->
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
            </div>
            <!-- Container End -->
        </div>
        <!-- Product Thumbnail Description End -->
        @if(count($productt->category->products) > 0)
        <!-- New Arrival Products Start Here -->
        <div class="new-arrival no-border-style ptb-90">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section-title text-center">
                    <h2>Related Products</h2>
                    <p>Add our new arrivals to your weekly lineup</p>
                </div>
                <!-- Section Title End -->
                <div class="our-pro-active owl-carousel">
                    @foreach($productt->category->products as $prod)
                    @if($productt->id != $prod->id)
                    <!-- Single Product Start Here -->
                    <div class="single-makal-product">
                    <a style="position:absolute; right:30px; top:20px; z-index:999;" id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($wishs->contains('product_id',$prod->id)) fa fa-heart @else fa fa-heart-o @endif"></i></a>
                        <div class="pro-img prods">
                            <a href="{{ route('front.product',$prod->slug) }}">
                                <img src="{{ $prod->thumbnail ? asset('public/assets/images/thumbnails/'.$prod->thumbnail):asset('public/assets/images/noimage.png') }}" alt="product-img">
                            </a>
                            <span class="sticker-new">new</span>
                            <div class="quick-view-pro">
                                <a data-toggle="modal" data-target="#product-window" class="quick-view" href="#"></a>
                            </div>
                        </div>
                        <div class="pro-content">
                            <h4 class="pro-title">
                                <a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName() }}</a>
                            </h4>
                            <p>
                                <span class="price">{{ $prod->showPrice() }}</span>
                            </p>
                            <div class="pro-actions">
                                <div class="actions-primary">
                                    <a data-href="{{ route('product.cart.add',$prod->id) }}" class="cart-btn add-to-cart" data-toggle="tooltip" data-original-title="Add to Cart">Add To Cart</a>
                                </div>
                                <div class="actions-secondary">
                                    {!! $prod->showRating() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Product End Here -->
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        <!-- New Arrival Products End Here -->
        @endif



@endsection

@section('scripts')
<script>
    $('.social-click').click(function(e){
        e.preventDefault();
        window.open($(this).attr('href'), '_blank');
    });

    $(document).ready(function(){
        $('.inc').click(function(e){
            var amount = $('#dynamic_price').attr('value');            
        });


    $("label.rating").click(function(){
	  $(this).parent().find("label").css({"background-color": "#78e2fb"});
	  $(this).css({"background-color": "#c7b270"});
	  $(this).nextAll().css({"background-color": "#c7b270"});

      var val = $(this).attr('id');
      $('#rating').val(val);
	});
 
	$(".star label").click(function(){
	  $(this).parent().find("label").css({"color": "#78e2fb"});
	  $(this).css({"color": "#c7b270"});
	  $(this).nextAll().css({"color": "#c7b270"});
	  $(this).css({"background-color": "transparent"});
	  $(this).nextAll().css({"background-color": "transparent"});
	});
    });

    $(document).ready(function(){
        $(function() {
            $('.zoom-image').each(function(){
            var originalImagePath = $(this).find('img').data('original-image');
            $(this).zoom({
            url: originalImagePath,
            magnify: 1
            });
         });

    });
}); 
</script>

@endsection