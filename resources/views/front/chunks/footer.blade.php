<footer class="pb-35">
<div class="container">
    <!-- Footer Middle Start -->
    <div class="footer-middle ptb-90">
        <div class="row">
            <!-- Single Footer Start -->
            <div class="col-lg-4 col-md-6 mb-all-30">
                <div class="single-footer">
                    <div class="footer-logo mb-20">
                        <a href="{{ route('front.index') }}">
                            <img style="height:70px;" src="{{asset('assets/images/'.$gs->footer_logo)}}" class="img" alt="footer-logo-image">
                        </a>
                    </div>
                    <div class="footer-content">
                        <ul class="footer-list first-content">
                            <li><i class="pe-7s-map-marker"></i>{{ $ps->street }}
                            </li>
                            <li><i class="pe-7s-call"></i>{{ $ps->phone }}</li>
                            <li><i class="pe-7s-print"></i>{{ $ps->fax }}</li>
                            <li><i class="pe-7s-clock"></i>{{ $ps->working_hours }}</li>
                            <li class="mt-20">
                                <ul class="social-icon">
                                @if(App\Models\Socialsetting::find(1)->f_status == 1)
                                    <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" class="facebook" target="_blank">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(App\Models\Socialsetting::find(1)->g_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->gplus }}" class="google-plus" target="_blank">
                                      <i class="fa fa-google-plus"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->t_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" class="twitter" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->l_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" class="linkedin" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->d_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->dribble }}" class="dribbble" target="_blank">
                                      <i class="fa fa-dribbble"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->i_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->instagram }}" class="dribbble" target="_blank">
                                      <i class="fa fa-instagram"></i>
                                  </a>
                                </li>
                                @endif
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Single Footer Start -->
            <!-- Single Footer Start -->
            <div class="col-lg-2 col-md-6 mb-all-30">
                <div class="single-footer">
                    <div class="single-footer">
                        <h4 class="footer-title">products</h4>
                        <div class="footer-content">
                            <ul class="footer-list">
                            @foreach($pages as $page)
                                @if($page->category == 'products' && $page->footer == 1)
                                <li><a href="{{route('front.page',$page->slug)}}">{{ $page->title }}</a></li>
                                @endif
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Footer Start -->
            <!-- Single Footer Start -->
            <div class="col-lg-2 col-md-6 mb-sm-30">
                <div class="single-footer">
                    <div class="single-footer">
                        <h4 class="footer-title">our company</h4>
                        <div class="footer-content">
                            <ul class="footer-list">
                                @foreach($pages as $page)
                                    @if($page->category == 'our company' && $page->footer == 1)
                                    <li><a href="{{route('front.page',$page->slug)}}">{{ $page->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Footer Start -->
            <!-- Single Footer Start -->
            <div class="col-lg-4 col-md-6">
                <div class="single-footer">
                    <div class="single-footer">
                        <h4 class="footer-title">Join Our Newsletter Now </h4>
                        <div class="footer-content subscribe-form">
                            <div class="subscribe-box">
                                @include('includes.admin.form-login')
                                <form id="subscribeform" action="{{ route('front.subscribe') }}" method="post">
                                @csrf
                                    <input type="text" name="email" placeholder="Enter Email Address" required />
                                    <button type="submit" class="pe-7s-mail-open"></button>
                                </form>
                            </div>
                            <p class="mt-10">Get E-mail updates about our latest shop and special offers.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Footer Start -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Footer Middle End -->
    <!-- Footer Bottom Start -->
    <div class="footer-bottom pt-35">
        <div class="col-md-12">
            <div class="row align-items-center justify-content-md-between">
                <div class="footer-copyright ">
                    {!! $gs->copyright !!}
                </div>
                <div class="footer-payment">
                    <a href="#"><img src="{{ asset('public/assets/front/img/payment/payment.png')}}" alt="payment-img" class="img"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom End -->
</div>
</footer>