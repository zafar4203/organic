        <!-- Main Header Area Three Start Here -->
        <header class="header-style-five">
            <!-- Header Top Start Here -->
            <div class="header-top">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row justify-content-lg-between justify-content-center">
                            <!-- Header Top Left Start -->
                            <div class="header-top-left order-2 order-lg-1">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fa fa-phone"></i> {{ $ps->phone }}</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-envelope-open-o"></i> {{ $ps->contact_email }}</a>
                                    </li>
                                    <li>
                                        <ul class="social-icon">
                                        @if(App\Models\Socialsetting::find(1)->f_status == 1)
                                    <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" class="facebook" target="_blank">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                @endif
                                @if(App\Models\Socialsetting::find(1)->g_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->gplus }}" class="google-plus" target="_blank">
                                      <i class="fa fa-google-plus"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->t_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" class="twitter" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->l_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" class="linkedin" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->d_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->dribble }}" class="dribbble" target="_blank">
                                      <i class="fa fa-dribbble"></i>
                                  </a>
                                </li>
                                @endif

                                @if(App\Models\Socialsetting::find(1)->i_status == 1)
                                <li>
                                  <a href="{{ App\Models\Socialsetting::find(1)->instagram }}" class="dribbble" target="_blank">
                                      <i class="fa fa-instagram"></i>
                                  </a>
                                </li>
                                @endif
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- Header Top Left End -->
                            <!-- Header Top Right Start -->
                            <div class="header-top-right order-1 order-lg-2">
                                <ul>
                                    <li>
                                        <a href="#">
                                            @if(Session::has('currency'))
                                                {{ DB::table('currencies')->where('id','=',Session::get('currency'))->first()->name }} {{DB::table('currencies')->where('id','=',Session::get('currency'))->first()->sign}}
                                            @else
                                                {{ DB::table('currencies')->where('is_default','=',1)->first()->name }} {{ DB::table('currencies')->where('is_default','=',1)->first()->sign }}
                                            @endif
                                            <i class="fa fa-angle-down"></i>
                                        </a>

                                        <!-- Dropdown Start -->
                                        <ul class="ht-dropdown">
                                            @foreach(DB::table('currencies')->get() as $currency)
											<li><a href="{{route('front.currency',$currency->id)}}">{{$currency->name}} {{$currency->sign}}</a></li>
    										@endforeach
                                        </ul>
                                        <!-- Dropdown End -->
                                    </li>
                                    <!-- <li>
                                        <a href="#">
                                            <img src="{{ asset('public/assets/front/img/header/1.jpg')}}" alt="language-selector">English
                                            <i class="fa fa-angle-down"></i>
                                        </a>

                                        <ul class="ht-dropdown">
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset('public/assets/front/img/header/1.jpg')}}" alt="language-selector">English</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset('public/assets/front/img/header/2.jpg')}}" alt="language-selector">Francis</a>
                                            </li>
                                        </ul>

                                    </li> -->
                                    <li>
                                        <a href="#">Setting
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <!-- Dropdown Start -->
                                        <ul class="ht-dropdown">
                                            <!-- <li>
                                                <a href="compare.html">compare products</a>
                                            </li> -->
                                            @if(Auth::check())
                                            <li>
                                                <a href="{{ route('user-dashboard') }}">my account</a>
                                            </li>
                                            @else
                                            <li>
                                                <a href="{{ route('user.login') }}">Sign in</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('user-register') }}">Register</a>
                                            </li>
                                            @endif
                                            <li>
                                                <a href="{{ route('user-wishlists') }}">my wishlist</a>
                                            </li>
                                            @if(Auth::check())
                                            <li>
                                                <a href="{{ route('user-logout') }}">Logout</a>
                                            </li>
                                            @endif
                                        </ul>
                                        <!-- Dropdown End -->
                                    </li>
                                </ul>
                            </div>
                            <!-- Header Top Right End -->
                        </div>
                    </div>
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End Here -->
            <!-- Header Middle Start Here -->
            <div class="header-middle stick header-sticky">
                <div class="header-middle_nav position-relative">
                    <div class="container">
                        <div class="row align-items-center">
                            <!-- Logo Start -->
                            <div class="col-xl-3 col-lg-2 col-6">
                                <div class="logo">
                                    <a href="{{ route('front.index') }}">
                                        <img style="height:70px;" src="{{asset('assets/images/'.$gs->logo)}}" class="img logo-img" alt="logo-image">
                                    </a>
                                </div>
                            </div>
                            <!-- Logo End -->
                            <!-- Menu Area Start Here -->
                            <div class="col-xl-7 col-lg-8 d-none d-lg-block position-static">
                                <nav>
                                    <ul class="header-bottom-list d-flex justify-content-start">
                                        @foreach($categories as $cat)
                                        <li class="active position-static">                                        
                                            <a @if(count($cat->subs) > 0) class="drop-icon" @endif href="{{ route('front.category',$cat->slug) }}">{{ $cat->name }}</a>
                                            <!-- Home Version Dropdown Start -->
                                            @if(count($cat->subs) > 0)
                                            <ul class="ht-dropdown">
                                                @foreach($cat->subs as $sub)
                                                <li>
                                                    <a href="{{ route('front.category',['$cat->slug , $sub->slug']) }}">{{ $sub->name}}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach

                                        @foreach($pages as $pg)
                                        @if($pg->header == 1)
                                        <li class="active position-static">                                        
                                            <a href="{{ route('front.page',$pg->slug) }}">
                                                {{ $pg->title }}
                                            </a>
                                        </li>
                                        @endif
                                        @endforeach
                                            <!-- Home Version Dropdown End -->
                                    </ul>
                                </nav>
                            </div>
                            <!-- Menu Area End Here -->
                            <!-- Cart Box Start Here -->
                            <div class="col-xl-2 col-lg-2 col-6">
                                <div class="cart-box">
                                    <ul>
                                        <!-- Search Box Start Here -->
                                        <li>
                                            <a href="javascript:;">
                                                <span class="pe-7s-search"></span>
                                            </a>
                                            <div class="categorie-search-box ht-dropdown">

        <form id="searchForm" action="{{ route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')]) }}" method="GET">
         @if (!empty(request()->input('sort')))
         <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
         @endif
         @if (!empty(request()->input('minprice')))
         <input type="hidden" name="minprice" value="{{ request()->input('minprice') }}">
         @endif
         @if (!empty(request()->input('maxprice')))
         <input type="hidden" name="maxprice" value="{{ request()->input('maxprice') }}">
         @endif
         <div class="form-search">
            <input  id="prod_name" name="search" placeholder="{{ $langg->lang2 }}" value="{{ request()->input('search') }}" autocomplete="off" type="text">                     
            <!-- <div class="autocomplete">
               <div id="myInputautocomplete-list" class="autocomplete-items"></div>
            </div> -->
            <button type="submit">
                <span class="pe-7s-search"></span>
            </button>
         </div>
      </form>


                                            </div>
                                        </li>
                                        <!-- Categorie Search Box End Here -->
                <li>
                    <a href="{{ route('front.cart') }}">
                        <span class="pe-7s-shopbag"></span>
                        <span id="cart-count" class="total-pro">{{ Session::has('cart') ? Session::get('cart')->totalCount() : '0' }}</span>
                    </a>
                    <ul class="ht-dropdown cart-box-width" id="cart-items">
                        @include('load.cart')
                    </ul>
                </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Cart Box End Here -->
                        </div>
                        <!-- Row End -->
                        <!-- Mobile Menu Start Here -->
                        <div class="mobile-menu d-block d-lg-none">
                            <nav>
                                <ul>
                                    <li>

                                        @foreach($categories as $cat)
                                            <a @if(count($cat->subs) > 0) class="drop-icon" @endif href="{{ route('front.category',$cat->slug) }}">{{ $cat->name }}</a>
                                            <!-- Home Version Dropdown Start -->
                                            @if(count($cat->subs) > 0)
                                            <ul class="ht-dropdown">
                                                @foreach($cat->subs as $sub)
                                                <li>
                                                    <a href="{{ route('front.category',['$cat->slug , $sub->slug']) }}">{{ $sub->name}}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        @endforeach

                                        @foreach($pages as $pg)
                                        @if($pg->header == 1)
                                            <a href="{{ route('front.page',$pg->slug) }}">
                                                {{ $pg->title }}
                                            </a>
                                        @endif
                                        @endforeach

                                    </li>
                                    <li>
                                        <a href="#">Currencies</a>
                                        <!-- Home Version Dropdown Start -->
                                        <ul>
                                            @foreach(DB::table('currencies')->get() as $currency)
                                            <li><a href="{{route('front.currency',$currency->id)}}">{{$currency->name}} {{$currency->sign}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Settings</a>
                                        <!-- Home Version Dropdown Start -->
                                        <ul>
                                        @if(Auth::check())
                                            <li>
                                                <a href="{{ route('user-dashboard') }}">my account</a>
                                            </li>
                                            @else
                                            <li>
                                                <a href="{{ route('user.login') }}">Sign in</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('user-register') }}">Register</a>
                                            </li>
                                            @endif
                                            <li>
                                                <a href="{{ route('user-wishlists') }}">my wishlist</a>
                                            </li>
                                            @if(Auth::check())
                                            <li>
                                                <a href="{{ route('user-logout') }}">Logout</a>
                                            </li>
                                            @endif

                                        </ul>
                                    </li>


                                </ul>
                            </nav>
                        </div>
                        <!-- Mobile Menu End Here -->
                    </div>
                    <!-- Container End -->
                </div>
            </div>
            <!-- Header Middle End Here -->
        </header>
        <!-- Main Header Area Three End Here -->
