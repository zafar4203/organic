@extends('layouts.front')

@section('title' , 'Login')

@section('styles')
    <style>
        .or-login{
            border:1px solid grey;
            padding:5px 10px;
        }
    </style>
@endsection

@section('content')
      <!-- Breadcrumb Area Start Here -->
      <div class="breadcrumb-area">
      <div class="container">
          <ol class="breadcrumb breadcrumb-list">
              <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Verify</li>
          </ol>
      </div>
  </div>
  <!-- Breadcrumb Area End Here -->


    <!-- Register Section Begin -->
    <div class="register-login-section spad mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="login-form signin-form">
                        <h2>Verify</h2>

                        @if(!session()->has('error'))
                            <div class="alert alert-info">
                                <p class="text-left">     
                                    Please Enter Verification Code Sent to you.         
                                </p> 
                            </div>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                        <button type="button" class="close alert-close"><span>×</span></button>
                            <p class="text-left">
                                {{ session()->get('error') }}
                            </p> 
                        </div>
                        @endif
                        <form action="{{ route('verify-account') }}" method="post">
                            @csrf
                            <div class="group-input">
                                <label for="username">Code to Verify*</label>
                                <input type="number" class="form-control" name="code" id="code" required>
                            </div>
                            <button type="submit" class="site-btn btn-block login-btn mt-3">Verify</button>
                        </form>
                        <div class="switch-login mt-3">
                            <a href="{{ route('user.login') }}" class="or-login">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->

@endsection
@section('scripts')
<script>

</script>
@endsection