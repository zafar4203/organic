@extends('layouts.front')
@section('title' , 'Login')
@section('styles')
  <style>
    .create-one{
        color:#e6bb34 !important;
    }
  </style>
@endsection

@section('content')
         <!-- Breadcrumb Area Start Here -->
         <div class="breadcrumb-area">
         <div class="container">
             <ol class="breadcrumb breadcrumb-list">
                 <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                 <li class="breadcrumb-item active">Login</li>
             </ol>
         </div>
     </div>
     <!-- Breadcrumb Area End Here -->




        <!-- Login Page Start Here -->
        <div class="login ptb-90">
            <div class="container">
                <h3 class="login-header text-center">Log in to your account </h3>
                <div class="row login-form-container">
                    <div class="col-xl-6 col-lg-8 offset-xl-3 offset-lg-2">
                        <div class="login-form signin-form">
                                @include('includes.admin.form-login')
                                <form class="mloginform" action="{{ route('user.login.submit') }}" method="post">
                                @csrf
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Enter Password" required>
                                        <button class="btn show-btn" type="button">Show</button>
                                    </div>
                                </div>
                                <div class="login-details text-center mb-25">
                                    <a href="{{route('user-forgot')}}">Forgot your password? </a>
                                    <button type="submit" class="login-btn">Sign in</button>
                                </div>
                                <div class="login-footer text-center">
                                    <p>No account? <a class="create-one" href="{{ route('user-register') }}">Create one here</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Login Page End Here -->


@endsection
@section('scripts')
<script>
    $('#log').click(function(){
        $('#rg').css('display','none');
        $('#lg').css('display','block');
    });

    $('#reg').click(function(){
        $('#rg').css('display','block');
        $('#lg').css('display','none');
    });

// const togglePassword = document.querySelector('#togglePassword');
        // const password = document.querySelector('#password');    

        // const ctogglePassword = document.querySelector('#confirmTogglePassword');
        // const cpassword = document.querySelector('#confirmPassword');    
        
        // togglePassword.addEventListener('click', function (e) {
        //     // toggle the type attribute
        //     const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        //     password.setAttribute('type', type);
        //     // toggle the eye slash icon
        //     this.classList.toggle('fa-eye-slash');
        // });
</script>
@endsection