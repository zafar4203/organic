@extends('layouts.front')
@section('title' , 'Register')
@section('styles')
    <style>
    
        #pswd-div i ,#confirm-pswd-div i {
            position:absolute;
            bottom:12px;
            right:15px;
            font-size:20px;
            cursor: pointer;
        }
        .create-one{
            color:#e6bb34 !important;
        }
        </style>
  
  
  
@endsection
@section('content')


     <!-- Breadcrumb Area Start Here -->
     <div class="breadcrumb-area">
            <div class="container">
                <ol class="breadcrumb breadcrumb-list">
                    <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Register</li>
                </ol>
            </div>
        </div>
        <!-- Breadcrumb Area End Here -->


        <!-- Regester Page Start Here -->
        <div class="register-area ptb-90">
            <div class="container">
                <h3 class="login-header">Create an account </h3>
                <div class="row">
                    <div class="offset-xl-1 col-xl-10">
                        <div class="register-form login-form clearfix signup-form">
                            @include('includes.admin.form-login')
                            <form id="registerform" action="{{route('user-register-submit')}}" method="post">
                            {{ csrf_field() }}

                                <p>Already have an account? <a class="create-one" href="{{ route('user.login') }}">Log in instead!</a></p>
                                <div class="form-group row">
                                    <label for="f-name" class="col-lg-3 col-md-3 col-form-label">First Name</label>
                                    <div class="col-lg-6 col-md-6">
                                    @if($errors->has('name'))
                                        <span class="red small-text">{{ $errors->first('name') }}</span>
                                    @endif
                                        <input type="text" name="name" class="form-control" placeholder="Enter Your Name" value="{{ old('name') }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-lg-3 col-md-3 col-form-label">Email</label>
                                    <div class="col-lg-6 col-md-6">
                                    @if($errors->has('email'))
                                        <span class="red small-text">{{ $errors->first('email') }}</span>
                                    @endif
                                    <input type="email" name="email" class="form-control" placeholder="Enter Email Address" value="{{ old('email') }}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-lg-3 col-md-3 col-form-label" for="email">Phone * </label>
                                @if($errors->has('phone'))
                                    <span class="red small-text">{{ $errors->first('phone') }}</span>
                                @endif

                                <div class="col-lg-6 col-md-6">
                                <div class="row">
                                    <div class="col-3 pr-0">
                                        <select name="country_code" class="form-control" id="">
                                            @foreach($countries as $country)
                                            <option value="{{$country->country_phone}}">{{ $country->country_code }} (+{{ $country->country_phone}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-9 pl-0">
                                        <input type="number" name="phone" class="form-control" placeholder="Enter Phone Number" value="{{ old('phone') }}" id="phone" required>
                                    </div>
                                </div>
                                </div>
                            </div>


                                <div class="form-group row">
                                    <label for="inputPassword" class="col-lg-3 col-md-3 col-form-label">Password</label>
                                    <div class="col-lg-6 col-md-6">
                                        @if($errors->has('password'))
                                            <span class="red small-text">{{ $errors->first('password') }}</span>
                                        @endif
                                        <input type="password" name="password" placeholder="Password" class="form-control" id="password" required>
                                        <button class="btn show-btn" id="togglePassword" type="button">Show</button>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputPassword" class="col-lg-3 col-md-3 col-form-label">Password</label>
                                    <div class="col-lg-6 col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" id="confirmPassword" required>
                                        <button class="btn show-btn" id="confirmTogglePassword" type="button">Show</button>
                                    </div>
                                </div>



                                <!-- <div class="form-check row p-0 mt-20">
                                    <div class="col-md-6 offset-md-3">
                                        <input class="form-check-input" value="#" id="offer" type="checkbox">
                                        <label class="form-check-label" for="offer">Receive offers from our
                                            partners</label>
                                    </div>
                                </div> -->
                                <div class="form-check row p-0 mt-20">
                                    <div class="col-md-8 offset-md-3">
                                        <input class="form-check-input" value="#" id="subscribe" type="checkbox">
                                        <label class="form-check-label" for="subscribe">Sign up for our
                                            newsletter<br>Subscribe to our newsletters now and stay up-to-date with new
                                            collections, the latest lookbooks and exclusive offers..</label>
                                    </div>
                                </div>
                                <div class="register-box mt-40">
                                    <button type="submit" class="login-btn float-right">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Register Page End Here -->


@endsection
@section('scripts')
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');    

        const ctogglePassword = document.querySelector('#confirmTogglePassword');
        const cpassword = document.querySelector('#confirmPassword');    
        
        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            // this.classList.toggle('fa-eye-slash');
        });


        ctogglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = cpassword.getAttribute('type') === 'password' ? 'text' : 'password';
            cpassword.setAttribute('type', type);
            // toggle the eye slash icon
            // this.classList.toggle('fa-eye-slash');
        });

        $('#submit').click(function(){
            if(isEmail($('#email').val())){                
                $('#submit').submit();
            }else{
                alert("Please provide valid Email");
            }
        });

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
            return regex.test(email);
        }
    </script>
@endsection