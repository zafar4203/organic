@if(Session::has('cart'))
<li>
<div class="row top-small-cart-menu">
	<div class="col-md-6 text-left">@if(Session::has('cart')) {{Session::get('cart')->totalCount()}} Item(s) @else 0 Item @endif</div>
	<div class="col-md-6 text-right"><a href="{{ route('front.cart') }}">View Cart</a></div>
</div>
<div class="cart-header-body">
@if(Session::has('cart') && Session::get('cart')->items)
@foreach(Session::get('cart')->items as $product)
	<!-- Cart Box Start -->
	<div class="single-cart-box product cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}">
		<div class="cart-img">
			<a href="{{ route('front.product',$product['item']['slug']) }}">
				<img src="{{ $product['item']['photo'] ? filter_var($product['item']['photo'], FILTER_VALIDATE_URL) ?$product['item']['photo']:asset('public/assets/images/products/'.$product['item']['photo']):asset('assets/images/noimage.png') }}" alt="product">
			</a>
			<span class="pro-quantity" id="cqt{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">{{$product['qty']}}X</span>
		</div>
		<div class="cart-content">
			<h6>
				<a href="{{ route('front.product',$product['item']['slug']) }}">{{mb_strlen($product['item']['name'],'utf-8') > 45 ? mb_substr($product['item']['name'],0,45,'utf-8').'...' : $product['item']['name']}}</a>
			</h6>
			<span class="cart-price">{{ App\Models\Product::convertPrice($product['item']['price']) }}</span>
			<!-- <span>Size: S</span>
			<span>Color: Yellow</span> -->
		</div>
		<a class="del-icone cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}" style="z-index:999" href="#">x
			<i class="ion-trash" ></i>
		</a>
	</div>
@endforeach
@endif

@if(Session::has('cart') && Session::get('cart')->offers)
@foreach(Session::get('cart')->offers as $offer)
	<!-- Cart Box Start -->
	<div class="single-cart-box product oremove{{ $offer['id'] }}">
		<div class="cart-img">
			<a href="javascript:;">
				@php 
					$offr = \App\Models\Offer::query()->where("id",$offer['id'])->first();
				@endphp                        
				<img src="{{ asset('public/assets/images/offers/'.$offr->image) }}" alt="{{ $offr->name }}">
			</a>
			<span class="pro-quantity">{{$offer['qty']}}X</span>
		</div>
		<div class="cart-content">
			<h6>
				<a href="javascript:;">{{mb_strlen($offr->name,'utf-8') > 45 ? mb_substr($offr->name,0,45,'utf-8').'...' : $offr->name}}</a>
			</h6>
			<span class="cart-price">{{ $offr->showPrice() }}</span>
			<!-- <span>Size: S</span>
			<span>Color: Yellow</span> -->
		</div>
		<a class="del-icone offerremovecart cart-offer-remove" data-class="oremove{{ $offer['id'] }}" data-href="{{ route('offer.cart.remove',$offer['id']) }}" style="z-index:999" href="javascript:;">x
			<i class="ion-trash" ></i>
		</a>
	</div>
@endforeach
@endif
</div>
	<!-- Cart Box End -->

	<!-- Cart Footer Inner Start -->
	<div class="mt-3 cart-footer">
		<ul class="price-content">
			<li>Total
				<span>{{ Session::has('cart') ? App\Models\Product::convertPrice(Session::get('cart')->totalPrice) : '0.00' }}</span>
			</li>
			<!-- <li>Shipping
				<span>$7.00</span>
			</li>
			<li>Taxes
				<span>$0.00</span>
			</li> -->
			<!-- <li>Total
				<span>$64.95</span>
			</li> -->
		</ul>
			<div class="cart-actions text-center">
				<a class="cart-checkout" href="{{ route('front.checkout') }}">Checkout</a>
			</div>
	</div>
	<!-- Cart Footer Inner End -->
</li>
@else 
<p class="mt-1 p-2 text-center">{{ $langg->lang8 }}</p>
@endif



