@extends('layouts.front')

@section('title' , 'Login')

@section('styles')
    <style>
  
    </style>
@endsection

@section('content')
      <!-- Breadcrumb Area Start Here -->
      <div class="breadcrumb-area">
      <div class="container">
          <ol class="breadcrumb breadcrumb-list">
              <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Forgot Password</li>
          </ol>
      </div>
  </div>
  <!-- Breadcrumb Area End Here -->

    <!-- Register Section Begin -->
    <div class="register-login-section spad mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="login-form signin-form">
                        <h2>Forgot Password</h2>
                        @include('includes.admin.form-login')
                        <form id="forgotform" action="{{route('user-forgot-submit')}}" method="POST">
                            @csrf
                            <div class="group-input mt-3">
                                <label for="username">Enter Email address *</label>
                                <input type="email" class="form-control" name="email" id="username" required>
                            </div>
                            <input class="authdata" type="hidden" value="{{ $langg->lang195 }}">
                            <button type="submit" class="mt-3 site-btn login-btn">Sign In</button>
                        </form>
                        <div class="switch-login">
                            <a href="{{ route('user.login') }}" class="or-login">Or Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->

@endsection

@section('scripts')
<script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');    

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });
</script>
@endsection