<ul class="nav flex-column dashboard-list" role="tablist">

                @php 

                  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
                  {
                    $link = "https"; 
                  }
                  else
                  {
                    $link = "http"; 
                      
                    // Here append the common URL characters. 
                    $link .= "://"; 
                      
                    // Append the host(domain name, ip) to the URL. 
                    $link .= $_SERVER['HTTP_HOST']; 
                      
                    // Append the requested resource location to the URL 
                    $link .= $_SERVER['REQUEST_URI']; 
                  }      

                @endphp
              <li>
                <a class="nav-link {{ $link == route('user-dashboard') ? 'active':'' }}" href="{{ route('user-dashboard') }}">
                  {{ $langg->lang200 }}
                </a>
              </li>
              
              <li>
                <a  class="nav-link {{ $link == route('user-orders') ? 'active':'' }}" href="{{ route('user-orders') }}">
                  {{ $langg->lang201 }}
                </a>
              </li>

              <!-- @if($gs->is_affilate == 1)

                <li class="{{ $link == route('user-affilate-code') ? 'active':'' }}">
                    <a href="{{ route('user-affilate-code') }}">{{ $langg->lang202 }}</a>
                </li>

                <li class="{{ $link == route('user-wwt-index') ? 'active':'' }}">
                    <a href="{{route('user-wwt-index')}}">{{ $langg->lang203 }}</a>
                </li>

              @endif -->


              <!-- <li class="{{ $link == route('user-order-track') ? 'active':'' }}">
                  <a href="{{route('user-order-track')}}">{{ $langg->lang772 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-messages') ? 'active':'' }}">
                  <a href="{{route('user-messages')}}">{{ $langg->lang232 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-message-index') ? 'active':'' }}">
                  <a href="{{route('user-message-index')}}">{{ $langg->lang204 }}</a>
              </li> -->

              <!-- <li class="{{ $link == route('user-dmessage-index') ? 'active':'' }}">
                  <a href="{{route('user-dmessage-index')}}">{{ $langg->lang250 }}</a>
              </li> -->

              <li>
                <a class="nav-link {{ $link == route('user-profile') ? 'active':'' }}" href="{{ route('user-profile') }}">
                  {{ $langg->lang205 }}
                </a>
              </li>

              <li>
                <a href="{{ route('user-reset') }}" class="nav-link {{ $link == route('user-reset') ? 'active':'' }}">
                 {{ $langg->lang206 }}
                </a>
              </li>

              <li>
                <a href="{{ route('user-wishlists') }}" class="nav-link {{ $link == route('user-wishlists') ? 'active':'' }}">
                  Wishlists
                </a>
              </li>

              <li>
                <a class="nav-link" href="{{ route('user-logout') }}">
                  {{ $langg->lang207 }}
                </a>
              </li>

      </ul>