<input type="hidden" value="{{ $prods->total() }}" id="total-pp" />
@if (count($prods) > 0)
	@foreach ($prods as $key => $prod)
<div class="col-lg-4 col-md-4 col-sm-6 col-6">
    <!-- Single Product Start Here -->
    <div class="single-makal-product">
     <a style="position:absolute; right:30px; top:20px; z-index:999;" id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}"><i class="wish_{{ $prod->id }} @if($wishs->contains('product_id',$prod->id)) fa fa-heart @else fa fa-heart-o @endif"></i></a>
        <div class="pro-img prods">
            <a href="{{ route('front.product',$prod->slug) }}">
                <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" alt="product-img">
            </a>
            @if($prod->new == 1)
            <span class="sticker-new">new</span>
            @endif
            @if($prod->dsc)
            <span class="sticker-sale">-{{ $prod->dsc }}%</span>
            @endif

            <div class="quick-view-pro">
                <a data-toggle="modal" href="javascript:;" title="Quick view" data-href="{{ route('product.quick',$prod->id) }}" data-target="#quickview" class="wish-view quick-a"></a>
            </div>
            <div class="quick-view-pro">
                <a data-toggle="modal" href="javascript:;" title="Quick view" data-href="{{ route('product.quick',$prod->id) }}" data-target="#quickview" class="quick-view quick-a"></a>
            </div>
        </div>
        <div class="pro-content">
            <h4 class="pro-title">
                <a href="{{ route('front.product',$prod->slug) }}">{{ $prod->showName() }}</a>
            </h4>

            <p>
                <span class="price">{{ $prod->setCurrency() }}</span>
            </p>
            <div class="pro-actions">
                <div class="actions-primary">
                    <a data-href="{{ route('product.cart.add',$prod->id) }}" class="cart-btn add-to-cart" data-toggle="tooltip" data-original-title="Add to Cart">Add To Cart</a>
                </div>
                <div class="actions-secondary">
                    {!! $prod->showRating() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Single Product End Here -->
</div>
@endforeach    

        <div class="col-lg-12">
            <div class="page-center mt-5">
                {!! $prods->appends(['search' => request()->input('search')])->links('pagination.default') !!}
            </div>
        </div>
@else
<div class="col-lg-12">
		<div class="page-center">
				<h4 class="text-center">{{ $langg->lang60 }}</h4>
		</div>
	</div>
@endif
 



@if(isset($ajax_check))


<script type="text/javascript">


// Tooltip Section


$('[data-toggle="tooltip"]').tooltip({
});
$('[data-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});




$('[rel-toggle="tooltip"]').tooltip();

$('[rel-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});


// Tooltip Section Ends

</script>

@endif