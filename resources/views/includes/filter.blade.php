                        <!-- Grid & List View Start -->
                        <div class="grid-list-top border-default universal-padding d-md-flex justify-content-md-between align-items-center mb-30">
                            <div class="grid-list-view d-flex align-items-center  mb-sm-15">
                                <!-- <ul class="nav tabs-area d-flex align-items-center">
                                    <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                class="fa fa-th"></i></a></li>
                                    <li><a data-toggle="tab" href="#list-view"><i class="fa fa-list-ul"></i></a></li>
                                </ul> -->
                                <span class="show-items">There Are <span id="show-total-pp">{{ $prods->total() }}</span> Products.</span>
                            </div>
                            <!-- Toolbar Short Area Start -->
                            <div class="main-toolbar-sorter clearfix">
                                <div class="toolbar-sorter d-md-flex align-items-center">
                                    <label>Sort By:</label>
                                    <select id="sortby" class="sorting sorter wide" name="sort">
                                        <option value="date_desc">{{$langg->lang65}}</option>
                                        <option value="date_asc">{{$langg->lang66}}</option>
                                        <option value="price_asc">{{$langg->lang67}}</option>
                                        <option value="price_desc">{{$langg->lang68}}</option>                    
                                    </select>
                                </div>
                            </div>
                            <!-- Toolbar Short Area End -->
                        </div>

