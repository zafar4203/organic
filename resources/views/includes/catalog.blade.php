    <!-- Sidebar Shopping Option Start -->
    <div class="col-lg-3 order-2 order-lg-1 mt-all-40">
        <div class="sidebar shop-sidebar">
            <!-- Price Filter Options Start -->
            @if(isset($_GET['search']) && !empty($_GET['search']))
            <div class="search-filter mb-30">
                <h3 class="sidebar-title">Search</h3>
                <a href="{{ route('front.category') }}"><span id="search-tag">{{ $_GET['search'] }} <i class="fa fa-times"></i></span></a>
            </div>
            @endif

            <div class="search-filter mb-30">
                <h3 class="sidebar-title">filter by price</h3>
                <form id="catalogForm" action="{{ route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')]) }}" method="GET">
                    @if (!empty(request()->input('search')))
                    <input type="hidden" name="search" value="{{ request()->input('search') }}">
                    @endif
                    @if (!empty(request()->input('sort')))
                    <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
                    @endif

                    <div id="slider-range"></div>
                    <input type="hidden" name="min" value="0" id="minamount" />
                    <input type="hidden" name="max" value="500" id="maxamount" />
                    <input type="text" id="amount" class="amount-range" readonly>
                    <div class="col text-center mt-3">
                        <button class="btn filter-btn btn-default" type="submit">{{$langg->lang58}}</button>
                    </div>
                </form>
            </div>
            <!-- Price Filter Options End -->
            <!-- Sidebar Categorie Start -->
            <div class="sidebar-categorie mb-30">
                <h3 class="sidebar-title">categories</h3>
                <ul class="sidbar-style">
                @php $catos = ""; 
                    if(isset($_GET['categories'])){
                        $catos = $_GET['categories'];
                    }
                @endphp
                @foreach ($categories as $element)

                    <li class="form-check">
                        <input class="form-check-input" name="cat[]" value="{{$element->id}}" {{in_array($element->id,explode(',',$catos))?'checked':''}} id="cat_{{$element->id}}" type="checkbox">
                        <label class="form-check-label" for="cat_{{$element->id}}">{{$element->name}} ({{ count($element->products) }})</label>
                    </li>

                @endforeach

                </ul>
            </div>
            <!-- Sidebar Categorie Start -->

@if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true))))
<form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">

@if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
      @foreach ($cat->attributes as $key => $attr)
            <!-- Product Size Start -->
            <div class="size mb-30">
                <h3 class="sidebar-title">{{$attr->name}}</h3>
                <ul class="size-list sidbar-style">
                @if (!empty($attr->attribute_options))
                  @foreach ($attr->attribute_options as $key => $option)
                    <li class="form-check">
                        <input class="form-check-input attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" type="checkbox">
                        <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                    </li>
                    @endforeach
                @endif  
                </ul>
            </div>
    @endforeach
@endif



@if (!empty($subcat) && !empty(json_decode($subcat->attributes, true)))
    @foreach ($subcat->attributes as $key => $attr)
            <!-- Product Size Start -->
            <div class="sidebar-categorie mb-30">
                <h3 class="sidebar-title">{{$attr->name}}</h3>
                <ul class="size-list sidbar-style">
                @if (!empty($attr->attribute_options))
                  @foreach ($attr->attribute_options as $key => $option)
                    <li class="form-check">
                        <input class="form-check-input attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" type="checkbox">
                        <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                    </li>
                    @endforeach
                @endif  
                </ul>
            </div>
    @endforeach
@endif


</form>
@endif

        </div>
    </div>
    <!-- Sidebar Shopping Option End -->
